class CreatePurchases < ActiveRecord::Migration[5.2]
  def change
    create_table :purchases do |t|
      t.string :title
      t.text :description
      t.boolean :buy
      t.references :shopping_list, foreign_key: true

      t.timestamps
    end
  end
end
