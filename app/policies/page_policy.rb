class PagePolicy < ApplicationPolicy
  def hello?
    true
  end

  def about_us?
    true
  end
end