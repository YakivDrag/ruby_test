class PurchasePolicy < ApplicationPolicy
  def index?
    false
  end

  def show?
    user.present? && user == purchase.shopping_list.user
  end

  def create?
    user.present?
  end

  def update?
    return true if user.present? && user == purchase.shopping_list.user
  end

  def destroy?
    return true if user.present? && user == purchase.shopping_list.user
  end

  private

  def purchase
    record
  end
end