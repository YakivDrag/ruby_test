class ShoppingListPolicy < ApplicationPolicy

  def index?
    user.present?
  end

  def show?
    return true if user.present? && user == shopping_list.user
  end

  def create?
    user.present?
  end

  def update?
    return true if user.present? && user == shopping_list.user
  end

  def destroy?
    return true if user.present? && user == shopping_list.user
  end

  private

  def shopping_list
    record
  end
end