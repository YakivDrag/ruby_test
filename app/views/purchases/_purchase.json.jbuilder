json.extract! purchase, :id, :title, :description, :buy, :shopping_list_id, :created_at, :updated_at
json.url purchase_url(purchase, format: :json)
