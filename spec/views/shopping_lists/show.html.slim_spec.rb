require 'rails_helper'

RSpec.describe "shopping_lists/show", type: :view do
  before(:each) do
    @shopping_list = assign(:shopping_list, ShoppingList.create!(
      :title => "Title",
      :description => "MyText",
      :user => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(//)
  end
end
