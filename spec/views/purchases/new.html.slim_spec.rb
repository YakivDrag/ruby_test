require 'rails_helper'

RSpec.describe "purchases/new", type: :view do
  before(:each) do
    assign(:purchase, Purchase.new(
      :title => "MyString",
      :description => "MyText",
      :buy => false,
      :shopping_list => nil
    ))
  end

  it "renders new purchase form" do
    render

    assert_select "form[action=?][method=?]", purchases_path, "post" do

      assert_select "input[name=?]", "purchase[title]"

      assert_select "textarea[name=?]", "purchase[description]"

      assert_select "input[name=?]", "purchase[buy]"

      assert_select "input[name=?]", "purchase[shopping_list_id]"
    end
  end
end
