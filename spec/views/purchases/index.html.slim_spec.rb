require 'rails_helper'

RSpec.describe "purchases/index", type: :view do
  before(:each) do
    assign(:purchases, [
      Purchase.create!(
        :title => "Title",
        :description => "MyText",
        :buy => false,
        :shopping_list => nil
      ),
      Purchase.create!(
        :title => "Title",
        :description => "MyText",
        :buy => false,
        :shopping_list => nil
      )
    ])
  end

  it "renders a list of purchases" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
