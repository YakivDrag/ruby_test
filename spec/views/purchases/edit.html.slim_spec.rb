require 'rails_helper'

RSpec.describe "purchases/edit", type: :view do
  before(:each) do
    @purchase = assign(:purchase, Purchase.create!(
      :title => "MyString",
      :description => "MyText",
      :buy => false,
      :shopping_list => nil
    ))
  end

  it "renders the edit purchase form" do
    render

    assert_select "form[action=?][method=?]", purchase_path(@purchase), "post" do

      assert_select "input[name=?]", "purchase[title]"

      assert_select "textarea[name=?]", "purchase[description]"

      assert_select "input[name=?]", "purchase[buy]"

      assert_select "input[name=?]", "purchase[shopping_list_id]"
    end
  end
end
