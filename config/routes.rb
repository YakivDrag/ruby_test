Rails.application.routes.draw do
  resources :purchases
  get 'purchases/:list_id', to: 'purchases#index'
  resources :shopping_lists
  devise_for :users
  get 'pages/about_us', to: 'pages#about_us'
  root 'pages#hello'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
